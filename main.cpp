#include <iostream>

#include "bank_manager.hh"
#include "BankService.h"

/**
 *  Bank accounts. 
 * 
 * At a bank, we have to keep track of the balance of some accounts. 
 * Also, each account has an associated log (the list of records of operations performed on that account). 
 * Each operation record shall have a unique serial number, that is incremented for each operation
 * performed in the bank. We have concurrently run transfer operations, to be executer on multiple threads.
 * Each operation transfers a given amount of money from one account to someother account, and also appends 
 * the information about the transfer to the logs of both accounts. 
 * From time to time, as well as at the end of the program, a consistency check shall be executed. 
 * It shall verify that the amount of money in each account corresponds with the operations records 
 * associated to that account, and also that all operations on each account appear also in the logs of 
 * the source or destination of the transfer.
 */

int main()
{
    std::vector < int > threadsConfig = { 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 4, 10, 10, 10};
    std::vector < int > bankNumber = {100, 1000000, 10, 100, 1000000, 10, 100, 1000000, 10, 100, 100000, 1000000, 10, 100, 1000000};

    auto bankService = new BankService(threadsConfig, bankNumber, "logs/details.txt");

    bankService->start();

    std::cout << "Done" << std::endl;

    return 0;
}