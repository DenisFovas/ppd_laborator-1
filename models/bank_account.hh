#pragma once

#include <vector>
#include <mutex>
#include <thread>

//long g_numberOfOperations = 0;

class BankAccount 
{
public:
    BankAccount();
    BankAccount(const BankAccount& bankAccount)
    {
        m_balance = bankAccount.getBalance();
        m_log = bankAccount.getLog();
    }
    
    void transferMoney(unsigned int amount, BankAccount &destinationAccount);
    
    unsigned int getBalance() const { return m_balance; }
    BankAccount* setBalance(unsigned int balance) { this->m_balance = balance; return this; }
    std::vector<int> getLog() const { return m_log; }

private:
    mutable std::mutex m_mutex;
    unsigned int m_balance;
    std::vector<int> m_log;
};