#include "./bank_account.hh"

BankAccount::BankAccount() 
{
    this->m_balance = 1000;
    this->m_log.push_back(0);
}

/**
 *
 * @param amount
 * @param destination
 */
void BankAccount::transferMoney(unsigned int amount, BankAccount &destination)
{
    std::lock_guard<std::mutex> lockGuard(m_mutex);

    if (this->m_balance -= amount < 0) {

        return;
    }

    this->m_balance -= amount;
    destination.setBalance(destination.getBalance() - amount);
    this->m_log.push_back(amount);
    destination.getLog().push_back(amount * (-1));
}