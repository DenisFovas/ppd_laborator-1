#include "bank_manager.hh"
#include "bank_account.hh"
#include <ctime>
#include <random>
#include <chrono>
#include <string>
#include <functional>

#define NUMBER_OF_OPERATIONS 100000

std::string return_current_time_and_date();

void BankManager::initiateRandomTransfer()
{
    while (m_numberOfOperations < m_maximumOperations) {
        std::function<unsigned int()> rnd = getRngEngine();

        unsigned int index = rnd() % m_numberOfAccounts;
        unsigned int indexSecond = rnd() % m_numberOfAccounts;

        if (indexSecond == index) {
            indexSecond = rnd() % m_numberOfAccounts - 1;
        }

        unsigned int amount = rnd() % 100;
        this->accounts[index].transferMoney(amount, this->accounts[indexSecond]);
        m_numberOfOperations++;
    }
}

/**
 * Define a random unsigned number generator.
 *
 * @return
 */
std::function<unsigned int()> BankManager::getRngEngine() const {
    std::uniform_int_distribution<unsigned int> uniformIntDistribution;
    std::random_device rd;
    std::mt19937 engine(rd());
    std::function<unsigned int()> rnd = std::bind(uniformIntDistribution, engine);

    return rnd;
}

void BankManager::start()
{
    auto startingTime = std::chrono::system_clock::now();

    std::thread threads[m_numberOfAccounts];

    for (size_t i = 0; i < (size_t) m_numberOfThreads; i++)
    {
        threads[i] = std::thread(&BankManager::initiateRandomTransfer, this);
    }
    
     for (size_t i = 0; i < (size_t) m_numberOfThreads; i++)
    {
        threads[i].join();
    }
}