//
// Created by denis on 10.10.2019.
//

#ifndef SRC_BANKSERVICE_H
#define SRC_BANKSERVICE_H

#include <utility>
#include <vector>
#include "bank_manager.hh"

class BankService
{
public:
    BankService(std::vector<int> threadsNumber, std::vector<int> bankNumber, std::string filename):
        m_threadNumber(std::move(threadsNumber)),
        m_numberOfBanks(std::move(bankNumber)),
        m_filename(std::move(filename))
    {

    }

    void start();
private:
    std::vector<BankManager> m_bankManagers;
    std::vector<int> m_threadNumber;
    std::vector<int> m_numberOfBanks;
    std::string m_filename;
};


#endif //SRC_BANKSERVICE_H
