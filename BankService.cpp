//
// Created by denis on 10.10.2019.
//

#include "BankService.h"
#include "bank_account.hh"

#include <iostream>

#define NUMBER_OF_OPERATIONS_PER_THREAD 100000


void BankService::start()
{
    for (auto i = m_threadNumber.begin(), j = m_numberOfBanks.begin(); i != m_threadNumber.end() && j != m_numberOfBanks.end(); i++, j++) {
        m_bankManagers.emplace_back(*i, *j, NUMBER_OF_OPERATIONS_PER_THREAD / *i);
    }

    for (auto item : m_bankManagers) {
        auto startingTime = std::chrono::system_clock::now();

        item.start();

        auto endingTime = std::chrono::system_clock::now();

        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(endingTime - startingTime).count();

        std::cout << "Difference: " << duration << "; "
                  << "Threads: " << std::to_string(item.getNumberOfThreads())  << "; "
                  << "Accounts: " << std::to_string(item.getAccounts().size()) << "; "
                  << "Operations: " << std::to_string(item.getNumberOfOperations());

        std::cout << std::endl;

    }
}
