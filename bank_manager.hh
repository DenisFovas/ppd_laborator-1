#pragma once

#include <utility>
#include <functional>

#include "./models/bank_account.hh"

class BankManager
{
public:
    BankManager(int8_t numberOfThreads, int numberOfAccounts, int maximOperations = 10):
        m_numberOfThreads(numberOfThreads), 
        m_numberOfAccounts(numberOfAccounts),
        m_maximumOperations(maximOperations)
    {
        for (int i = 0; i < m_numberOfAccounts; i++)
        {
            accounts.emplace_back();
            m_numberOfOperations = 0;
        }
    }

    std::vector<BankAccount> getAccounts() const { return accounts; }
    int getNumberOfThreads() const { return m_numberOfThreads; }
    int getNumberOfOperations() const { return m_numberOfOperations; }

    void start();
protected:
    int m_numberOfOperations;
    int m_maximumOperations;
    std::vector<BankAccount> accounts;
    int8_t m_numberOfThreads;
    int m_numberOfAccounts;
    
    void initiateRandomTransfer();
    std::function<unsigned int()> getRngEngine() const;
};